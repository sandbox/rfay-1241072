<?php

/**
 * @file
 *   Commerce File License migration.
 *   Customized to bring in licenses from uc_file_s3
 */
class CommerceMigrateUbercartUserMigration extends Migration {
  public function __construct() {
    // The basic setup is similar to BeerTermMigraiton
    parent::__construct();
    $this->description = t('Users from another drupal db');
    $this->map = new MigrateSQLMap($this->machineName,
        array('uid' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'uid.'
                )
             ),
        MigrateDestinationUser::getKeySchema()
    );

    $connection = commerce_migrate_ubercart_get_source_connection();
    $query = $connection->select('users', 'u');
    $query->condition('uid', 1, '>');
    $query->fields('u', array('uid', 'name', 'pass', 'mail', 'mode', 'created', 'access', 'login', 'status', 'timezone', 'language', 'picture'));
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationUser(array('md5_passwords' => TRUE));

    // Mapped fields
    $this->addSimpleMappings(array('pass', 'mail', 'created', 'access', 'login', 'status', 'timezone', 'language', 'picture'));
    $this->addFieldMapping('name', 'name')
         ->dedupe('users', 'name');
    $this->addFieldMapping('roles')
         ->defaultValue(2);

    // Unmapped destination fields

    // This is a shortcut you can use to mark several destination fields as DNM
    // at once
    $this->addUnmigratedDestinations(array('theme', 'signature', 'is_new'));
  }
}